-- -------------------------------------------------------------
-- 
-- File Name: C:\Users\jito\Desktop\simulink\hdl\hdlsrc\testidemohomma\Audio_Fil_ip_src_Audio_Filter_pkg.vhd
-- Created: 2021-11-01 21:07:50
-- 
-- Generated by MATLAB 9.9 and HDL Coder 3.17
-- 
-- -------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

PACKAGE Audio_Fil_ip_src_Audio_Filter_pkg IS
  TYPE vector_of_unsigned24 IS ARRAY (NATURAL RANGE <>) OF unsigned(23 DOWNTO 0);
  TYPE vector_of_signed24 IS ARRAY (NATURAL RANGE <>) OF signed(23 DOWNTO 0);
  TYPE vector_of_unsigned8 IS ARRAY (NATURAL RANGE <>) OF unsigned(7 DOWNTO 0);
END Audio_Fil_ip_src_Audio_Filter_pkg;

