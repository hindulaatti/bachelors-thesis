function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <S1>/Valid_in */
	this.urlHashMap["testidemohomma:4"] = "msg=rtwMsg_notTraceable&block=testidemohomma:4";
	/* <S1>/Effect_block */
	this.urlHashMap["testidemohomma:16"] = "Audio_Fil_ip_src_Audio_Filter.vhd:88,89,90,91,92,93,94,95,96,97";
	/* <S7>/Data_Type_Conversion_In */
	this.urlHashMap["testidemohomma:38"] = "Audio_Fil_ip_src_Effect_block.vhd:90,91,92,93";
	/* <S7>/Data_Type_Conversion_Out */
	this.urlHashMap["testidemohomma:48"] = "Audio_Fil_ip_src_Effect_block.vhd:95,96,97,98";
	/* <S7>/Delay */
	this.urlHashMap["testidemohomma:166"] = "msg=&block=testidemohomma:166";
	/* <S7>/Gain */
	this.urlHashMap["testidemohomma:191"] = "Audio_Fil_ip_src_Effect_block.vhd:191,192";
	/* <S7>/Multiport
Switch */
	this.urlHashMap["testidemohomma:55"] = "Audio_Fil_ip_src_Effect_block.vhd:240,241,242,243";
	/* <S7>/Product */
	this.urlHashMap["testidemohomma:182"] = "msg=&block=testidemohomma:182";
	/* <S7>/Product1 */
	this.urlHashMap["testidemohomma:189"] = "Audio_Fil_ip_src_Effect_block.vhd:208";
	/* <S7>/Saturation */
	this.urlHashMap["testidemohomma:192"] = "Audio_Fil_ip_src_Effect_block.vhd:159,160,161";
	/* <S8>/Bit Shift */
	this.urlHashMap["testidemohomma:40"] = "Audio_Fil_ip_src_Data_Type_Conversion_In.vhd:60,61,62";
	/* <S8>/Bit Slice */
	this.urlHashMap["testidemohomma:41"] = "Audio_Fil_ip_src_Data_Type_Conversion_In.vhd:46";
	/* <S8>/Bit Slice1 */
	this.urlHashMap["testidemohomma:42"] = "Audio_Fil_ip_src_Data_Type_Conversion_In.vhd:48";
	/* <S8>/Data Type Conversion */
	this.urlHashMap["testidemohomma:43"] = "Audio_Fil_ip_src_Data_Type_Conversion_In.vhd:54,55,56";
	/* <S8>/Demux */
	this.urlHashMap["testidemohomma:44"] = "Audio_Fil_ip_src_Data_Type_Conversion_In.vhd:65";
	/* <S8>/Mux */
	this.urlHashMap["testidemohomma:45"] = "Audio_Fil_ip_src_Data_Type_Conversion_In.vhd:50,51";
	/* <S8>/Sum */
	this.urlHashMap["testidemohomma:46"] = "Audio_Fil_ip_src_Data_Type_Conversion_In.vhd:69";
	/* <S9>/Bit Concat */
	this.urlHashMap["testidemohomma:50"] = "Audio_Fil_ip_src_Data_Type_Conversion_Out.vhd:55";
	/* <S9>/Concatenation */
	this.urlHashMap["testidemohomma:51"] = "Audio_Fil_ip_src_Data_Type_Conversion_Out.vhd:41,42";
	/* <S9>/Data Type Conversion1 */
	this.urlHashMap["testidemohomma:52"] = "Audio_Fil_ip_src_Data_Type_Conversion_Out.vhd:45,46,47,48";
	/* <S9>/Demux1 */
	this.urlHashMap["testidemohomma:53"] = "Audio_Fil_ip_src_Data_Type_Conversion_Out.vhd:51";
	/* <S10>/bit_shift */
	this.urlHashMap["testidemohomma:40:481"] = "msg=rtwMsg_notTraceable&block=testidemohomma:40:481";
	/* <S11>/DTProp1 */
	this.urlHashMap["testidemohomma:41:1698"] = "msg=rtwMsg_notTraceable&block=testidemohomma:41:1698";
	/* <S11>/Reinterp_As_Unsigned_Integer */
	this.urlHashMap["testidemohomma:41:1700"] = "msg=rtwMsg_notTraceable&block=testidemohomma:41:1700";
	/* <S12>/DTProp1 */
	this.urlHashMap["testidemohomma:42:1698"] = "msg=rtwMsg_notTraceable&block=testidemohomma:42:1698";
	/* <S12>/Reinterp_As_Unsigned_Integer */
	this.urlHashMap["testidemohomma:42:1700"] = "msg=rtwMsg_notTraceable&block=testidemohomma:42:1700";
	/* <S13>:1 */
	this.urlHashMap["testidemohomma:40:481:1"] = "msg=rtwMsg_optimizedSfObject&block=testidemohomma:40:481:1";
	/* <S14>/DTProp1 */
	this.urlHashMap["testidemohomma:41:1703:2"] = "msg=rtwMsg_notTraceable&block=testidemohomma:41:1703:2";
	/* <S14>/DTProp2 */
	this.urlHashMap["testidemohomma:41:1703:3"] = "msg=rtwMsg_notTraceable&block=testidemohomma:41:1703:3";
	/* <S14>/Extract Desired Bits */
	this.urlHashMap["testidemohomma:41:1703:4"] = "msg=rtwMsg_notTraceable&block=testidemohomma:41:1703:4";
	/* <S14>/Modify Scaling Only */
	this.urlHashMap["testidemohomma:41:1703:5"] = "msg=rtwMsg_notTraceable&block=testidemohomma:41:1703:5";
	/* <S15>/DTProp1 */
	this.urlHashMap["testidemohomma:42:1703:2"] = "msg=rtwMsg_notTraceable&block=testidemohomma:42:1703:2";
	/* <S15>/DTProp2 */
	this.urlHashMap["testidemohomma:42:1703:3"] = "msg=rtwMsg_notTraceable&block=testidemohomma:42:1703:3";
	/* <S15>/Extract Desired Bits */
	this.urlHashMap["testidemohomma:42:1703:4"] = "msg=rtwMsg_notTraceable&block=testidemohomma:42:1703:4";
	/* <S15>/Modify Scaling Only */
	this.urlHashMap["testidemohomma:42:1703:5"] = "msg=rtwMsg_notTraceable&block=testidemohomma:42:1703:5";
	/* <S16>/Bit Concat2 */
	this.urlHashMap["testidemohomma:50:2"] = "msg=rtwMsg_notTraceable&block=testidemohomma:50:2";
	/* <S17>/bit_concat */
	this.urlHashMap["testidemohomma:50:2:106"] = "msg=rtwMsg_notTraceable&block=testidemohomma:50:2:106";
	/* <S18>:1 */
	this.urlHashMap["testidemohomma:50:2:106:1"] = "msg=rtwMsg_optimizedSfObject&block=testidemohomma:50:2:106:1";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "testidemohomma"};
	this.sidHashMap["testidemohomma"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>/Data_in"] = {sid: "testidemohomma:3"};
	this.sidHashMap["testidemohomma:3"] = {rtwname: "<S1>/Data_in"};
	this.rtwnameHashMap["<S1>/Valid_in"] = {sid: "testidemohomma:4"};
	this.sidHashMap["testidemohomma:4"] = {rtwname: "<S1>/Valid_in"};
	this.rtwnameHashMap["<S1>/Filter_select"] = {sid: "testidemohomma:5"};
	this.sidHashMap["testidemohomma:5"] = {rtwname: "<S1>/Filter_select"};
	this.rtwnameHashMap["<S1>/Post_gain"] = {sid: "testidemohomma:156"};
	this.sidHashMap["testidemohomma:156"] = {rtwname: "<S1>/Post_gain"};
	this.rtwnameHashMap["<S1>/Pre_gain"] = {sid: "testidemohomma:179"};
	this.sidHashMap["testidemohomma:179"] = {rtwname: "<S1>/Pre_gain"};
	this.rtwnameHashMap["<S1>/Effect_block"] = {sid: "testidemohomma:16"};
	this.sidHashMap["testidemohomma:16"] = {rtwname: "<S1>/Effect_block"};
	this.rtwnameHashMap["<S1>/Data_out"] = {sid: "testidemohomma:57"};
	this.sidHashMap["testidemohomma:57"] = {rtwname: "<S1>/Data_out"};
	this.rtwnameHashMap["<S1>/Valid_out"] = {sid: "testidemohomma:58"};
	this.sidHashMap["testidemohomma:58"] = {rtwname: "<S1>/Valid_out"};
	this.rtwnameHashMap["<S7>/Data_in"] = {sid: "testidemohomma:17"};
	this.sidHashMap["testidemohomma:17"] = {rtwname: "<S7>/Data_in"};
	this.rtwnameHashMap["<S7>/Input to Switch"] = {sid: "testidemohomma:21"};
	this.sidHashMap["testidemohomma:21"] = {rtwname: "<S7>/Input to Switch"};
	this.rtwnameHashMap["<S7>/Post_gain"] = {sid: "testidemohomma:155"};
	this.sidHashMap["testidemohomma:155"] = {rtwname: "<S7>/Post_gain"};
	this.rtwnameHashMap["<S7>/Pre_gain"] = {sid: "testidemohomma:178"};
	this.sidHashMap["testidemohomma:178"] = {rtwname: "<S7>/Pre_gain"};
	this.rtwnameHashMap["<S7>/Enable"] = {sid: "testidemohomma:22"};
	this.sidHashMap["testidemohomma:22"] = {rtwname: "<S7>/Enable"};
	this.rtwnameHashMap["<S7>/Data_Type_Conversion_In"] = {sid: "testidemohomma:38"};
	this.sidHashMap["testidemohomma:38"] = {rtwname: "<S7>/Data_Type_Conversion_In"};
	this.rtwnameHashMap["<S7>/Data_Type_Conversion_Out"] = {sid: "testidemohomma:48"};
	this.sidHashMap["testidemohomma:48"] = {rtwname: "<S7>/Data_Type_Conversion_Out"};
	this.rtwnameHashMap["<S7>/Delay"] = {sid: "testidemohomma:166"};
	this.sidHashMap["testidemohomma:166"] = {rtwname: "<S7>/Delay"};
	this.rtwnameHashMap["<S7>/Gain"] = {sid: "testidemohomma:191"};
	this.sidHashMap["testidemohomma:191"] = {rtwname: "<S7>/Gain"};
	this.rtwnameHashMap["<S7>/Multiport Switch"] = {sid: "testidemohomma:55"};
	this.sidHashMap["testidemohomma:55"] = {rtwname: "<S7>/Multiport Switch"};
	this.rtwnameHashMap["<S7>/Product"] = {sid: "testidemohomma:182"};
	this.sidHashMap["testidemohomma:182"] = {rtwname: "<S7>/Product"};
	this.rtwnameHashMap["<S7>/Product1"] = {sid: "testidemohomma:189"};
	this.sidHashMap["testidemohomma:189"] = {rtwname: "<S7>/Product1"};
	this.rtwnameHashMap["<S7>/Saturation"] = {sid: "testidemohomma:192"};
	this.sidHashMap["testidemohomma:192"] = {rtwname: "<S7>/Saturation"};
	this.rtwnameHashMap["<S7>/Data_out"] = {sid: "testidemohomma:56"};
	this.sidHashMap["testidemohomma:56"] = {rtwname: "<S7>/Data_out"};
	this.rtwnameHashMap["<S8>/In1"] = {sid: "testidemohomma:39"};
	this.sidHashMap["testidemohomma:39"] = {rtwname: "<S8>/In1"};
	this.rtwnameHashMap["<S8>/Bit Shift"] = {sid: "testidemohomma:40"};
	this.sidHashMap["testidemohomma:40"] = {rtwname: "<S8>/Bit Shift"};
	this.rtwnameHashMap["<S8>/Bit Slice"] = {sid: "testidemohomma:41"};
	this.sidHashMap["testidemohomma:41"] = {rtwname: "<S8>/Bit Slice"};
	this.rtwnameHashMap["<S8>/Bit Slice1"] = {sid: "testidemohomma:42"};
	this.sidHashMap["testidemohomma:42"] = {rtwname: "<S8>/Bit Slice1"};
	this.rtwnameHashMap["<S8>/Data Type Conversion"] = {sid: "testidemohomma:43"};
	this.sidHashMap["testidemohomma:43"] = {rtwname: "<S8>/Data Type Conversion"};
	this.rtwnameHashMap["<S8>/Demux"] = {sid: "testidemohomma:44"};
	this.sidHashMap["testidemohomma:44"] = {rtwname: "<S8>/Demux"};
	this.rtwnameHashMap["<S8>/Mux"] = {sid: "testidemohomma:45"};
	this.sidHashMap["testidemohomma:45"] = {rtwname: "<S8>/Mux"};
	this.rtwnameHashMap["<S8>/Sum"] = {sid: "testidemohomma:46"};
	this.sidHashMap["testidemohomma:46"] = {rtwname: "<S8>/Sum"};
	this.rtwnameHashMap["<S8>/Out1"] = {sid: "testidemohomma:47"};
	this.sidHashMap["testidemohomma:47"] = {rtwname: "<S8>/Out1"};
	this.rtwnameHashMap["<S9>/In2"] = {sid: "testidemohomma:160"};
	this.sidHashMap["testidemohomma:160"] = {rtwname: "<S9>/In2"};
	this.rtwnameHashMap["<S9>/Bit Concat"] = {sid: "testidemohomma:50"};
	this.sidHashMap["testidemohomma:50"] = {rtwname: "<S9>/Bit Concat"};
	this.rtwnameHashMap["<S9>/Concatenation"] = {sid: "testidemohomma:51"};
	this.sidHashMap["testidemohomma:51"] = {rtwname: "<S9>/Concatenation"};
	this.rtwnameHashMap["<S9>/Data Type Conversion1"] = {sid: "testidemohomma:52"};
	this.sidHashMap["testidemohomma:52"] = {rtwname: "<S9>/Data Type Conversion1"};
	this.rtwnameHashMap["<S9>/Demux1"] = {sid: "testidemohomma:53"};
	this.sidHashMap["testidemohomma:53"] = {rtwname: "<S9>/Demux1"};
	this.rtwnameHashMap["<S9>/Out1"] = {sid: "testidemohomma:54"};
	this.sidHashMap["testidemohomma:54"] = {rtwname: "<S9>/Out1"};
	this.rtwnameHashMap["<S10>/u"] = {sid: "testidemohomma:40:480"};
	this.sidHashMap["testidemohomma:40:480"] = {rtwname: "<S10>/u"};
	this.rtwnameHashMap["<S10>/bit_shift"] = {sid: "testidemohomma:40:481"};
	this.sidHashMap["testidemohomma:40:481"] = {rtwname: "<S10>/bit_shift"};
	this.rtwnameHashMap["<S10>/y"] = {sid: "testidemohomma:40:482"};
	this.sidHashMap["testidemohomma:40:482"] = {rtwname: "<S10>/y"};
	this.rtwnameHashMap["<S11>/u"] = {sid: "testidemohomma:41:1697"};
	this.sidHashMap["testidemohomma:41:1697"] = {rtwname: "<S11>/u"};
	this.rtwnameHashMap["<S11>/DTProp1"] = {sid: "testidemohomma:41:1698"};
	this.sidHashMap["testidemohomma:41:1698"] = {rtwname: "<S11>/DTProp1"};
	this.rtwnameHashMap["<S11>/Extract Bits"] = {sid: "testidemohomma:41:1703"};
	this.sidHashMap["testidemohomma:41:1703"] = {rtwname: "<S11>/Extract Bits"};
	this.rtwnameHashMap["<S11>/Reinterp_As_Unsigned_Integer"] = {sid: "testidemohomma:41:1700"};
	this.sidHashMap["testidemohomma:41:1700"] = {rtwname: "<S11>/Reinterp_As_Unsigned_Integer"};
	this.rtwnameHashMap["<S11>/y"] = {sid: "testidemohomma:41:1701"};
	this.sidHashMap["testidemohomma:41:1701"] = {rtwname: "<S11>/y"};
	this.rtwnameHashMap["<S12>/u"] = {sid: "testidemohomma:42:1697"};
	this.sidHashMap["testidemohomma:42:1697"] = {rtwname: "<S12>/u"};
	this.rtwnameHashMap["<S12>/DTProp1"] = {sid: "testidemohomma:42:1698"};
	this.sidHashMap["testidemohomma:42:1698"] = {rtwname: "<S12>/DTProp1"};
	this.rtwnameHashMap["<S12>/Extract Bits"] = {sid: "testidemohomma:42:1703"};
	this.sidHashMap["testidemohomma:42:1703"] = {rtwname: "<S12>/Extract Bits"};
	this.rtwnameHashMap["<S12>/Reinterp_As_Unsigned_Integer"] = {sid: "testidemohomma:42:1700"};
	this.sidHashMap["testidemohomma:42:1700"] = {rtwname: "<S12>/Reinterp_As_Unsigned_Integer"};
	this.rtwnameHashMap["<S12>/y"] = {sid: "testidemohomma:42:1701"};
	this.sidHashMap["testidemohomma:42:1701"] = {rtwname: "<S12>/y"};
	this.rtwnameHashMap["<S13>:1"] = {sid: "testidemohomma:40:481:1"};
	this.sidHashMap["testidemohomma:40:481:1"] = {rtwname: "<S13>:1"};
	this.rtwnameHashMap["<S14>/u"] = {sid: "testidemohomma:41:1703:1"};
	this.sidHashMap["testidemohomma:41:1703:1"] = {rtwname: "<S14>/u"};
	this.rtwnameHashMap["<S14>/DTProp1"] = {sid: "testidemohomma:41:1703:2"};
	this.sidHashMap["testidemohomma:41:1703:2"] = {rtwname: "<S14>/DTProp1"};
	this.rtwnameHashMap["<S14>/DTProp2"] = {sid: "testidemohomma:41:1703:3"};
	this.sidHashMap["testidemohomma:41:1703:3"] = {rtwname: "<S14>/DTProp2"};
	this.rtwnameHashMap["<S14>/Extract Desired Bits"] = {sid: "testidemohomma:41:1703:4"};
	this.sidHashMap["testidemohomma:41:1703:4"] = {rtwname: "<S14>/Extract Desired Bits"};
	this.rtwnameHashMap["<S14>/Modify Scaling Only"] = {sid: "testidemohomma:41:1703:5"};
	this.sidHashMap["testidemohomma:41:1703:5"] = {rtwname: "<S14>/Modify Scaling Only"};
	this.rtwnameHashMap["<S14>/y"] = {sid: "testidemohomma:41:1703:6"};
	this.sidHashMap["testidemohomma:41:1703:6"] = {rtwname: "<S14>/y"};
	this.rtwnameHashMap["<S15>/u"] = {sid: "testidemohomma:42:1703:1"};
	this.sidHashMap["testidemohomma:42:1703:1"] = {rtwname: "<S15>/u"};
	this.rtwnameHashMap["<S15>/DTProp1"] = {sid: "testidemohomma:42:1703:2"};
	this.sidHashMap["testidemohomma:42:1703:2"] = {rtwname: "<S15>/DTProp1"};
	this.rtwnameHashMap["<S15>/DTProp2"] = {sid: "testidemohomma:42:1703:3"};
	this.sidHashMap["testidemohomma:42:1703:3"] = {rtwname: "<S15>/DTProp2"};
	this.rtwnameHashMap["<S15>/Extract Desired Bits"] = {sid: "testidemohomma:42:1703:4"};
	this.sidHashMap["testidemohomma:42:1703:4"] = {rtwname: "<S15>/Extract Desired Bits"};
	this.rtwnameHashMap["<S15>/Modify Scaling Only"] = {sid: "testidemohomma:42:1703:5"};
	this.sidHashMap["testidemohomma:42:1703:5"] = {rtwname: "<S15>/Modify Scaling Only"};
	this.rtwnameHashMap["<S15>/y"] = {sid: "testidemohomma:42:1703:6"};
	this.sidHashMap["testidemohomma:42:1703:6"] = {rtwname: "<S15>/y"};
	this.rtwnameHashMap["<S16>/u1"] = {sid: "testidemohomma:50:31"};
	this.sidHashMap["testidemohomma:50:31"] = {rtwname: "<S16>/u1"};
	this.rtwnameHashMap["<S16>/u2"] = {sid: "testidemohomma:50:32"};
	this.sidHashMap["testidemohomma:50:32"] = {rtwname: "<S16>/u2"};
	this.rtwnameHashMap["<S16>/Bit Concat2"] = {sid: "testidemohomma:50:2"};
	this.sidHashMap["testidemohomma:50:2"] = {rtwname: "<S16>/Bit Concat2"};
	this.rtwnameHashMap["<S16>/y"] = {sid: "testidemohomma:50:34"};
	this.sidHashMap["testidemohomma:50:34"] = {rtwname: "<S16>/y"};
	this.rtwnameHashMap["<S17>/u1"] = {sid: "testidemohomma:50:2:104"};
	this.sidHashMap["testidemohomma:50:2:104"] = {rtwname: "<S17>/u1"};
	this.rtwnameHashMap["<S17>/u2"] = {sid: "testidemohomma:50:2:105"};
	this.sidHashMap["testidemohomma:50:2:105"] = {rtwname: "<S17>/u2"};
	this.rtwnameHashMap["<S17>/bit_concat"] = {sid: "testidemohomma:50:2:106"};
	this.sidHashMap["testidemohomma:50:2:106"] = {rtwname: "<S17>/bit_concat"};
	this.rtwnameHashMap["<S17>/y"] = {sid: "testidemohomma:50:2:107"};
	this.sidHashMap["testidemohomma:50:2:107"] = {rtwname: "<S17>/y"};
	this.rtwnameHashMap["<S18>:1"] = {sid: "testidemohomma:50:2:106:1"};
	this.sidHashMap["testidemohomma:50:2:106:1"] = {rtwname: "<S18>:1"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
