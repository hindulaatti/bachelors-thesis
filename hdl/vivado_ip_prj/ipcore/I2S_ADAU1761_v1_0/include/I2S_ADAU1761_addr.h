/* Copyright 2017 The MathWorks, Inc. */
/*
 * File Name:         hdl_prj\ipcore\I2S_ADAU1761_v1_0\include\I2S_ADAU1761_addr.h
 * Description:       C Header File
 * Created:           2017-01-05 18:14:02
*/

#ifndef I2S_ADAU1761_H_
#define I2S_ADAU1761_H_

#define  IPCore_Reset_I2S_ADAU1761                           0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_I2S_ADAU1761                          0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_PacketSize_AXI4_Stream_Master_I2S_ADAU1761   0x8  //Packet size for AXI4-Stream Master interface, the default value is 1024. The TLAST output signal of the AXI4-Stream Master interface is generated based on the packet size.

#endif /* I2S_ADAU1761_H_ */
