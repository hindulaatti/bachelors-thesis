/*
 * File Name:         hdl_prj\ipcore\I2C_ADAU1761_v1_0\include\I2C_ADAU1761_addr.h
 * Description:       C Header File
 * Created:           2017-10-27 11:33:01
*/

#ifndef I2C_ADAU1761_H_
#define I2C_ADAU1761_H_

#define  IPCore_Reset_I2C_ADAU1761       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_I2C_ADAU1761      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_I2C_ADAU1761   0x8  //contains unique IP timestamp (yymmddHHMM): 1710271132

#endif /* I2C_ADAU1761_H_ */
