/*
 * File Name:         C:\Users\jito\Desktop\simulink\hdl\ipcore\Audio_Fil_ip_v1_0\include\Audio_Fil_ip_addr.h
 * Description:       C Header File
 * Created:           2021-11-01 21:07:54
*/

#ifndef AUDIO_FIL_IP_H_
#define AUDIO_FIL_IP_H_

#define  IPCore_Reset_Audio_Fil_ip                           0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_Audio_Fil_ip                          0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_PacketSize_AXI4_Stream_Master_Audio_Fil_ip   0x8  //Packet size for AXI4-Stream Master interface, the default value is 1024. The TLAST output signal of the AXI4-Stream Master interface is generated based on the packet size.
#define  IPCore_Timestamp_Audio_Fil_ip                       0xC  //contains unique IP timestamp (yymmddHHMM): 2111012107
#define  Filter_select_Data_Audio_Fil_ip                     0x100  //data register for Inport Filter_select
#define  Post_gain_Data_Audio_Fil_ip                         0x104  //data register for Inport Post_gain
#define  Pre_gain_Data_Audio_Fil_ip                          0x108  //data register for Inport Pre_gain

#endif /* AUDIO_FIL_IP_H_ */
