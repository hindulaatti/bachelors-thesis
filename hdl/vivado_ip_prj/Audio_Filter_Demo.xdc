# ZedBoard
# 24 mhz clock to audio chip
set_property PACKAGE_PIN AB2 [get_ports Clk_24MHz]
set_property IOSTANDARD LVCMOS33 [get_ports Clk_24MHz]

# I2S interface

set_property PACKAGE_PIN Y8 [get_ports Serial_data_out]
set_property IOSTANDARD LVCMOS33 [get_ports Serial_data_out]

set_property PACKAGE_PIN AA7 [get_ports Serial_data_in]
set_property IOSTANDARD LVCMOS33 [get_ports Serial_data_in]

set_property PACKAGE_PIN AA6 [get_ports Bit_clock]
set_property IOSTANDARD LVCMOS33 [get_ports Bit_clock]

set_property PACKAGE_PIN Y6 [get_ports Left_Right_select]
set_property IOSTANDARD LVCMOS33 [get_ports Left_Right_select]

# I2C Data Interface to ADAU1761 (for configuration)
set_property PACKAGE_PIN AB4 [get_ports I2C_CLK]
set_property IOSTANDARD LVCMOS33 [get_ports I2C_CLK]

set_property PACKAGE_PIN AB5 [get_ports I2C_DATA]
set_property IOSTANDARD LVCMOS33 [get_ports I2C_DATA]

set_property PACKAGE_PIN AB1 [get_ports ADDR0]
set_property IOSTANDARD LVCMOS33 [get_ports ADDR0]

set_property PACKAGE_PIN Y5 [get_ports ADDR1]
set_property IOSTANDARD LVCMOS33 [get_ports ADDR1]

